from django.shortcuts import render, redirect
from django.contrib.auth import login, logout
from django.contrib.auth.hashers import make_password
from django.contrib.auth.models import User
from .models import PracticalTaskModel, PracticalUserWorkModel
from user_input_app.models import VariantUserModel
import time


def test(request, key):
    if request.user.is_authenticated:  # Проверка на авторизованность

        check_admin = False
        if request.user.is_superuser:
            check_admin = True

        error = ''  # Информация об ошибках
        codeform = ''  # Код студента для формы
        if request.method == "POST":
            # Создаю в БД работу студента с его данными и кодом программы
            id_user = User.objects.get(username=request.user.username)  # Студент
            input_field = request.POST.get("input_field")  # Его отправленный код
            data_word = PracticalTaskModel.objects.get(pk=key)  # Данные о практической работе
            user_work = PracticalUserWorkModel.objects.create(
                text_code=input_field,
                user=id_user,
                practical=data_word,
                course=data_word.course,
                subject=data_word.subject,
                number=data_word.number,
                variant=data_word.variant,
            )

            user_work.save()
            user_work = PracticalUserWorkModel.objects.filter(practical_id=key, user=id_user.id).order_by('-id')[0]

            # В code хранится отправленный код студента (str)
            code = user_work.text_code

            test_work = PracticalTaskModel.objects.get(pk=key)
            code_test = test_work.text_code

            number_test = 0
            lines = code_test.split('\n')
            all_in = []
            all_out = []

            for line in lines:
                if line[0] != '\n':
                    input_arguments = []
                    output_arguments = []

                    tmp = ''

                    for i in range(len(line)):
                        if line[i] == '=':
                            separator = i

                    for i in range(len(line)):
                        if (i < separator):
                            if (line[i] != ' ') and (line[i] != ';'):
                                tmp += line[i]

                            if (line[i] == ';') or (line[i] == ' '):
                                if (tmp != ''):
                                    tmp = tmp.replace("−", "-")
                                    if ("-" in tmp or tmp.isdigit()):
                                        tmp = int(tmp)
                                    input_arguments.append(tmp)
                                tmp = ''

                        if (i > separator):
                            if (line[i - 1] == '=') or (line[i - 1] == ';'):
                                continue
                            if (line[i] != ';') and (line[i] != '\n') and (i != len(line)) \
                                and (line[i] != '\r'):
                                tmp += line[i]

                            # if (line[i] == ';') or (line[i] == ' ') or (line[i] == '\n') or (i+1 == len(line)):
                            if (line[i] == ';') or (line[i] == '\n') or (i+1 == len(line)):
                                if (tmp != ''):
                                    tmp = tmp.replace("−", "-")
                                    if ("-" in tmp or tmp.isdigit()):
                                        tmp = int(tmp)
                                    output_arguments.append(tmp)
                                tmp = ''
                    number_test += 1
                all_in.append(input_arguments)
                all_out.append(output_arguments)

            time.sleep(1)

            if not code:
                error = "Вставьте ваш код"
                user_work.delete()
            else:
                try:
                    exec(code)
                    if 'import' in code:
                        raise ImportError

                    try:
                        for num in range(len(all_in)):
                            if len(all_out[num]) == 1:
                                assert vars()["main"](*all_in[num]) == all_out[num][0], str(num + 1)
                            else:
                                assert vars()["main"](*all_in[num]) == all_out[num], str(num + 1)

                    except BaseException:
                        raise

                    time.sleep(2)
                    error = "Все тесты пройдены. Молодец!"

                    # Создание первой записи успешно сданной работы
                    user_work = PracticalUserWorkModel.objects.filter(practical_id=key, user=id_user.id)
                    user_work.update(mark=True)
                    # Если студент уже проходил успешно решал эту задачу и есть в БД, тогда
                    # будет перезапись его старого успешного кода на новый успешный код
                    if PracticalUserWorkModel.objects.filter(practical_id=key, user=id_user.id).count() > 1:
                        PracticalUserWorkModel.objects.filter(practical_id=key, user=id_user.id, mark=False).delete()
                        time.sleep(2)
                        user_work = PracticalUserWorkModel.objects.filter(practical_id=key, user=id_user.id, mark=True)
                        user_work.update(text_code=code)

                    # Удаление повторных True сданных работ юзером из БД
                    while True:
                        if PracticalUserWorkModel.objects.filter(practical_id=key, user=id_user.id, mark=True).count() > 1:
                            time.sleep(2)
                            user_work = PracticalUserWorkModel.objects.filter(practical_id=key, user=id_user.id, mark=True).order_by('-id')[0]
                            user_work.delete()
                        else:
                            break

                except TimeoutError:
                    PracticalUserWorkModel.objects.filter(practical_id=key, user=id_user.id, mark=False).delete()
                    error = "Количество 'while True' и 'break' не совпадает"

                except TypeError:
                    PracticalUserWorkModel.objects.filter(practical_id=key, user=id_user.id, mark=False).delete()
                    error = "Не все аргументы переданы"

                except AssertionError as num_fail:
                    PracticalUserWorkModel.objects.filter(practical_id=key, user=id_user.id, mark=False).delete()
                    error = f"Тест {num_fail}: Не пройден"

                except ImportError:
                    PracticalUserWorkModel.objects.filter(practical_id=key, user=id_user.id, mark=False).delete()
                    error = "Импорт запрещён"

                except KeyError:
                    PracticalUserWorkModel.objects.filter(practical_id=key, user=id_user.id, mark=False).delete()
                    error = "Отсутствует функция main"

                except SyntaxError:
                    PracticalUserWorkModel.objects.filter(practical_id=key, user=id_user.id, mark=False).delete()
                    error = "Ошибка синтаксиса"

                except BaseException as gg:
                    PracticalUserWorkModel.objects.filter(practical_id=key, user=id_user.id, mark=False).delete()
                    error = f"Не знаю что за ошибка, Python пишет: {gg}"

        work = PracticalTaskModel.objects.get(pk=key)  # Загрузка выбранной по id практической

        # Тут я обрабатываю данные для примера входных и выходных данных на странице
        example = work.text_code.split('\n')
        arguments = ["Ошибка", "Ошибка"]
        if len(example) >= 1 and "=" in example[0] and (";" in example[0] or "," in example[0]):
            arguments = [elem.replace(";", ",") for elem in example[0].split(' = ')]
            if arguments[0].count(',') == 1 and arguments[0][-1] == ',':
                arguments[0] = arguments[0].replace(",", "")
        arguments.append(arguments[1].count(','))

        if request.method == "POST":
            codeform = input_field

        data = {
            "arguments_input": arguments[0],
            "arguments_exit": arguments[1],
            "amount_arguments_exit": arguments[2],
            "codeform": codeform,
            "error": error,
            'work': work,
            "check_admin": check_admin,
        }
        return render(request, "user_panel_app/index.html", context=data)
    else:
        return redirect('auth/')


def practical_all(request, key):
    if request.user.is_authenticated:  # Проверка на авторизованность

        check_admin = False
        if request.user.is_superuser:
            check_admin = True

        index1 = (key-1) * 10
        index2 = key * 10

        bd_op = ''
        amount_note = PracticalTaskModel.objects.all().count() / 10
        bd = PracticalTaskModel.objects.all().order_by('course', 'number', 'variant')[index1:index2]

        # Поиск работы по id
        if request.method == "POST" and "btn_search" in request.POST:
            bd = PracticalTaskModel.objects.all()
            id_search = request.POST.get("id_search")
            bd = bd.filter(pk=id_search)

        if request.method == "POST" and "btn_mine" in request.POST:
            return redirect('/')

        # Фильтрация
        if request.method == "POST" and "btn_fil" in request.POST:
            bd = PracticalTaskModel.objects.all().order_by('course', 'number', 'variant')
            course = request.POST.get("course")
            subject = request.POST.get("subject")
            number = request.POST.get("number")
            variant = request.POST.get("variant")

            # Фильтрация по полям
            if course != 'Курс':
                bd = bd.filter(course__contains=course)
            if subject != 'Предмет':
                bd = bd.filter(subject__contains=subject)
            if number != '':
                bd = bd.filter(number__contains=number)
            if variant != '':
                bd = bd.filter(variant__contains=variant)

        data = {
            "bd": bd,
            "bd_op": bd_op,
            "key": key,
            "amount_note": amount_note,
            "check_admin": check_admin,
        }
        return render(request, "user_panel_app/practical.html", context=data)
    else:
        return redirect('auth/')


def practical_my(request):
    if request.user.is_authenticated and not request.user.is_superuser:
        check_admin = False

        id_user = User.objects.get(username=request.user.username)
        mine_variant = VariantUserModel.objects.get(user=id_user.id)  # Получение его варианта и курса
        # Ниже загрузка практических студента по его СОБСТВЕННОМУ варианту по ВВПД и ОП и курсу
        bd = PracticalTaskModel.objects.filter(variant=mine_variant.variant_vvpd, subject="ВВПД", course=mine_variant.course)
        bd_op = PracticalTaskModel.objects.filter(variant=mine_variant.variant_op, subject="ОП", course=mine_variant.course)

        # Если нажимается кнопка "Все работы"
        if request.method == "POST" and "btn_all" in request.POST:
            return redirect('../practical_all/1/')

        # Рабочая фильтрация
        if request.method == "POST" and "btn_fil" in request.POST:
            course = request.POST.get("course")
            subject = request.POST.get("subject")
            number = request.POST.get("number")
            variant = request.POST.get("variant")

            # Фильтрация по полям
            if request.POST.get("course") != 'Курс':
                bd = bd.filter(course__contains=course)
                bd_op = bd_op.filter(course__contains=course)
            if request.POST.get("subject") != 'Предмет':
                bd = bd.filter(subject__contains=subject)
                bd_op = bd_op.filter(subject__contains=subject)
            if request.POST.get("number") != '':
                bd = bd.filter(number__contains=number)
                bd_op = bd_op.filter(number__contains=number)
            if request.POST.get("variant") != '':
                bd = bd.filter(variant__contains=variant)
                bd_op = bd_op.filter(variant__contains=variant)

        data = {
            "bd": bd,
            "bd_op": bd_op,
            "username": request.user.username,
            "last_name": request.user.last_name,
            "check_admin": check_admin,
        }
        return render(request, "user_panel_app/practical.html", context=data)
    else:
        return redirect('auth/')


def profile(request):
    if request.user.is_authenticated and not request.user.is_superuser:
        error = ''

        user = User.objects.get(username=request.user.username)  # Получение данных студента
        mine_data = VariantUserModel.objects.get(user=user.id)  # Получение его варианта
        if request.method == "POST" and "save_btn" in request.POST:
            user.first_name = request.POST.get("first_name")
            user.last_name = request.POST.get("last_name")
            mine_data.course = request.POST.get("course")
            mine_data.variant_vvpd = request.POST.get("variant_vvpd")
            mine_data.variant_op = request.POST.get("variant_op")
            user.save()
            mine_data.save()
            error = 'Данные сохранены'

        if request.method == "POST" and "password_btn" in request.POST:
            password1 = request.POST.get("password1")
            password2 = request.POST.get("password2")
            error = 'Пароли не совпадают'
            if password1 == password2:
                if len(password1) > 7:
                    user.password = make_password(password1)
                    print(password1)
                    user.save()
                    logout(request)
                    login(request, user)
                    error = 'Пароль изменён'
                else:
                    error = 'Ваш пароль меньше 8 символов'

        mine_works = PracticalUserWorkModel.objects.filter(user=user.id)

        data = {
            "error": error,
            "user": user,
            "mine_data": mine_data,
            "mine_works": mine_works,
        }
        return render(request, "user_panel_app/profile.html", context=data)
    else:
        return redirect('auth/')
