from django.db import models
from django.contrib.auth.models import User


class PracticalTaskModel(models.Model):
    CATEGORY1 = (
        ('ОП', 'ОП'),
        ("ВВПД", "ВВПД")
    )
    CATEGORY2 = (
        (1, '1'),
        (2, "2"),
        (3, "3"),
        (4, "4"),
    )

    course = models.IntegerField("Курс", choices=CATEGORY2)
    subject = models.CharField("Предмет", max_length=25, choices=CATEGORY1)
    number = models.IntegerField("Номер практической")
    variant = models.IntegerField("Вариант")
    condition = models.TextField("Условие задачи")
    text_code = models.TextField("Тесты")

    def __str__(self):
        return 'Практическая работа'

    class Meta:
        verbose_name_plural = 'Добавить/редактировать практическую'
        verbose_name = 'работу'


class PracticalUserWorkModel(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE, blank=True, null=True)
    practical = models.ForeignKey(PracticalTaskModel, on_delete=models.CASCADE, blank=True, null=True)
    text_code = models.TextField("", blank=True, null=True)
    date = models.DateTimeField("Дата", auto_now=True)
    mark = models.BooleanField("", default=False)
    course = models.IntegerField("Курс", blank=True, null=True)
    subject = models.CharField("Предмет", max_length=25, blank=True, null=True)
    number = models.IntegerField("Номер практической", blank=True, null=True)
    variant = models.IntegerField("Вариант", blank=True, null=True)

    def __str__(self):
        return '%s %s' % (self.user.first_name, self.user.last_name)

    class Meta:
        verbose_name = 'Программы студентов'
        verbose_name_plural = 'Код студентов'
