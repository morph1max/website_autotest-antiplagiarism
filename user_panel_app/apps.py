from django.apps import AppConfig


class UserPanelAppConfig(AppConfig):
    name = 'user_panel_app'
    verbose_name = 'Меню практических работ'
