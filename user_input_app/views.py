from django.shortcuts import render
from django.shortcuts import redirect
from .forms import PersonInput, PersonRegister
from .models import VariantUserModel
from django.contrib.auth.models import User
from django.contrib.auth import login, authenticate, logout


def auth(request):
    logout(request)  # Юзер при переходе на эту страницу разлогинивается

    error_input = ''
    if request.method == "POST" and 'btn_reg' in request.POST:
        return redirect('../register/')

    # Обработка входа
    if request.method == "POST" and 'btn_auth' in request.POST:
        username = request.POST.get("username")
        password = request.POST.get("password")
        user = authenticate(username=username, password=password)
        if user is not None:
            login(request, user)
            if user.is_superuser is True:
                return redirect('../admin/')
            return redirect('/')
        error_input = "Неверный логин или пароль"

    # Создание форм регистрации и входа
    userform = PersonInput()
    data = {
        "form_input": userform,
        "error_input": error_input,
    }
    return render(request, "user_input_app/index_log.html", context=data)


def register(request):
    logout(request)  # Юзер при переходе на эту страницу разлогинивается

    error_register = ''

    if request.method == "POST" and 'btn_back' in request.POST:
        return redirect('../auth/')

    # Обработка регистрации
    if request.method == "POST" and 'btn_reg' in request.POST:
        human = PersonRegister(request.POST)
        if human.is_valid():
            gooduser = human.save()
            gooduser.variantusermodel_set.create()  # Добавление юзера в новую БД с ЛИЧНЫМИ вариантами

            # Делаю имя и фамилию всегда с большой буквы
            update_human = User.objects.filter(username=request.POST.get("username"))
            update_human.update(first_name=request.POST.get("first_name").capitalize(), last_name=request.POST.get("last_name").capitalize())

            login(request, gooduser)  # Авторизация юзера
            return redirect('/')
        # Если ошибки в регистрации
        else:
            username = request.POST.get("username")
            if User.objects.filter(username=username).count() > 0:
                error_register = "Логин занят"
            if error_register == '':
                error_register = "Логин и пароль должны быть не похожи. Пароль - минимум 8 символов из букв"

    # Создание форм регистрации
    reguserform = PersonRegister()
    data = {
        "form_reg": reguserform,
        "error_register": error_register,
    }
    return render(request, "user_input_app/index_reg.html", context=data)
