from django.db import models
from django.contrib.auth.models import User


class VariantUserModel(models.Model):
    user = models.ForeignKey(User, verbose_name="Логин студента", on_delete=models.CASCADE, unique=True)
    variant_vvpd = models.IntegerField("Вариант по ВВПД", null=True, blank=True)
    variant_op = models.IntegerField("Вариант по ОП", null=True, blank=True)
    course = models.IntegerField("Курс", null=True, blank=True)

    def __str__(self):
        return '%s %s' % (self.user.first_name, self.user.last_name)

    class Meta:
        verbose_name_plural = 'Изменить вариант или курс'
        verbose_name = 'вариант'
