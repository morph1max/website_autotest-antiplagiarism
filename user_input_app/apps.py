from django.apps import AppConfig


class UserInputAppConfig(AppConfig):
    name = 'user_input_app'
    verbose_name = 'Меню вариантов студентов'
