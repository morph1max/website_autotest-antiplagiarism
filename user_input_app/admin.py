from django.contrib import admin
from .models import VariantUserModel


class VariantUserModelAdmin(admin.ModelAdmin):
    list_display = ('user', )
    search_fields = ('user__username', )

    # Небольшое украшение форм
    def render_change_form(self, request, context, *args, **kwargs):
        form_instance = context['adminform'].form
        form_instance.fields['variant_vvpd'].widget.attrs['placeholder'] = 'Номер'
        form_instance.fields['variant_op'].widget.attrs['placeholder'] = 'Номер'
        form_instance.fields['course'].widget.attrs['placeholder'] = 'Курс'
        return super().render_change_form(request, context, *args, **kwargs)


admin.site.register(VariantUserModel, VariantUserModelAdmin)
