"""autotests URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from user_input_app import views as v1
from user_panel_app import views as v2
from admin_antiplag_app import views as v3


urlpatterns = [
    path('auth/', v1.auth, name='auth'),
    path('register/', v1.register, name='register'),
    path('antiplag/', v3.antiplag, name='antiplag'),
    path('practical_all/<int:key>/', v2.practical_all, name='practical_all'),
    path('profile/', v2.profile, name='profile'),
    path('students/', v3.students, name='students'),
    path('', v2.practical_my, name='practical_my'),
    path('test/<int:key>/', v2.test, name='test'),
    path('admin/', admin.site.urls),
]
