from django.apps import AppConfig


class AdminAntiplagAppConfig(AppConfig):
    name = 'admin_antiplag_app'
