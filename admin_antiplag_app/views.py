from django.shortcuts import render
from user_panel_app.models import PracticalUserWorkModel
from user_input_app.models import VariantUserModel
from django.contrib.auth.models import User
from django.shortcuts import redirect
import time


def antiplag(request):
    if request.user.is_authenticated:
        if request.user.is_superuser:  # Проверка на авторизованность
            error = ''
            work1 = ''
            work2 = ''
            id_one_labs = ''  # Список id самой схожей работы
            about_practical = ('', '', '', '')
            percent_labs = []  # Список схожести всех работ в % и сами работы

            if request.method == "POST" and "btn_ant" in request.POST:
                course = request.POST.get("course")
                subject = request.POST.get("subject")
                number = request.POST.get("number")
                variant = request.POST.get("variant")
                about_practical = (course, subject, number, variant)

                # в works хранятся все успешно сданные работы студентов конкретной практической
                # На выбор админа
                works = PracticalUserWorkModel.objects.filter(course=course, subject=subject, number=number, variant=variant)

                if works.count() > 1:
                    id_all_labs = []  # Список id всех работ

                    arg_all = []  # список в котором список со всеми аргументами
                    count = 0  # количество схожих аргументов
                    all_c_lines = []

                    for work in works:
                        # work - одна из всех работ

                        new_code = []  # список из аргументов текущей работы
                        tmp_list = []  # список для составления слова из списка символов
                        tmp = ''  # строка - аргумент
                        c_lines = 1 # счетчик строк

                        for i in range(len(work.text_code)):
                            if work.text_code[i] == '\n':
                                c_lines += 1

                            if ((ord(work.text_code[i]) > 64) and (ord(work.text_code[i]) < 91)) \
                                or ((ord(work.text_code[i]) > 96) and (ord(work.text_code[i]) < 123)):
                                # проверка на английские символы
                                tmp_list.append(work.text_code[i])

                            else:
                                if tmp_list:
                                    # составление слова (аргумента) из списка символов
                                    tmp = tmp.join(tmp_list)

                                if tmp and (tmp not in new_code) and (tmp != "def") and (tmp != "main") and (tmp != "return"):
                                    # проверка символа, чтобы не повторялся и добавление в список аргументов
                                    new_code.append(tmp)

                                tmp_list = []
                                tmp = ''

                        arg_all.append(new_code)  # Добавляем в список переменные одной работы
                        id_all_labs.append(work.id)  # Поочерёдно записываем id работ в общий список
                        all_c_lines.append(c_lines)  # Добавление в список количетсва строк

                    # Сравнение всех работ конкретной практики из works
                    for index1 in range(works.count()):
                        for index2 in range(index1+1, works.count()):
                            test1 = arg_all[index1]
                            test2 = arg_all[index2]

                            if all_c_lines[index1] <= all_c_lines[index2]:
                                some_number = all_c_lines[index1] / all_c_lines[index2]
                            elif all_c_lines[index1] > all_c_lines[index2]:
                                some_number = all_c_lines[index2] / all_c_lines[index1]

                            if len(test1) >= len(test2):
                                for i in range(len(test1) - len(test2)):
                                    # для сравнения списком нужно сделать их одинаковой длинны
                                    test2.append(-1)

                            else:
                                for k in range(len(test2) - len(test1)):
                                    # для сравнения списком нужно сделать их одинаковой длинны
                                    test1.append(-1)

                            for g in range(len(test1)):
                                if test1[g] in test2:
                                    # подсчет количетсва схожих аргументов
                                    count += 1

                            percent = int(((count * 100) / len(test1)) * some_number)
                            count = 0
                            percent_labs.append([percent])

                    # В список добавляю два id работ и их схожесть - [percent, id_1, id_2]...
                    amount = 0
                    for work_id1 in range(len(id_all_labs)):
                        for work_id2 in range(work_id1+1, len(id_all_labs)):
                            percent_labs[amount].append(id_all_labs[work_id1])
                            percent_labs[amount].append(id_all_labs[work_id2])
                            tmp_work1 = PracticalUserWorkModel.objects.get(pk=percent_labs[amount][1])
                            tmp_work2 = PracticalUserWorkModel.objects.get(pk=percent_labs[amount][2])

                            tmp_user1 = User.objects.get(username=tmp_work1.user)
                            tmp_user2 = User.objects.get(username=tmp_work2.user)
                            percent_labs[amount].append(tmp_user1)
                            percent_labs[amount].append(tmp_user2)
                            amount += 1

                    if len(percent_labs) > 2:
                        temp_percent = []  # Тут только хранятся проценты
                        for el in percent_labs:
                            temp_percent.append(el[0])

                        # Нахожу самый большой процент среди работ
                        percent_max = max(temp_percent)
                        index_max = temp_percent.index(percent_max)
                        id_one_labs = percent_labs[index_max]
                        percent_labs.pop(index_max)

                        # Две самые схожие работы между собой
                        work1 = PracticalUserWorkModel.objects.get(pk=id_one_labs[1])
                        work2 = PracticalUserWorkModel.objects.get(pk=id_one_labs[2])
                        id_one_labs[1] = work1
                        id_one_labs[2] = work2
                    else:
                        id_one_labs = percent_labs[0]
                        work1 = PracticalUserWorkModel.objects.get(pk=id_one_labs[1])
                        work2 = PracticalUserWorkModel.objects.get(pk=id_one_labs[2])
                        id_one_labs[1] = work1
                        id_one_labs[2] = work2
                else:
                    error = 'Сданных работ меньше двух'
                time.sleep(1)
                if len(percent_labs) > 5:
                    percent_labs = percent_labs[0:5]
                else:
                    percent_labs = percent_labs[0:len(percent_labs)]
                percent_labs.sort()
                percent_labs.reverse()

            data = {
                "all": percent_labs,
                "work": id_one_labs,
                "about": about_practical,
                "error": error,
            }
            return render(request, "admin_antiplag_app/antiplag.html", context=data)
        else:
            return redirect('/')
    else:
        return redirect('/')


def students(request):
    if request.user.is_authenticated and request.user.is_superuser:
        error = ''
        user = ''
        mine_data = ''
        mine_works = ''

        if request.method == "POST" and "btn_search_1" in request.POST:
            username = request.POST.get("username")
            if User.objects.filter(username=username):
                user = User.objects.get(username=username)
                mine_data = VariantUserModel.objects.get(user=user.id)
                mine_works = PracticalUserWorkModel.objects.filter(user=user.id)
            else:
                error = "Пользователь не найден"

        if request.method == "POST" and "btn_search_2" in request.POST:
            first_name = request.POST.get("first_name").capitalize()
            last_name = request.POST.get("last_name").capitalize()
            if User.objects.filter(first_name=first_name, last_name=last_name):
                user = User.objects.get(first_name=first_name, last_name=last_name)
                mine_data = VariantUserModel.objects.get(user=user.id)
                mine_works = PracticalUserWorkModel.objects.filter(user=user.id)
            else:
                error = "Пользователь не найден"

        data = {
            "error": error,
            "user": user,
            "mine_data": mine_data,
            "mine_works": mine_works,
        }
        return render(request, "admin_antiplag_app/students.html", context=data)
    else:
        return redirect('auth/')
